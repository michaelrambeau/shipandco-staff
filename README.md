# shipandco-staff

The dashboard for Ship&Co staff.

Single page application made with [`create-react-app`](https://github.com/facebook/create-react-app).

## Setup

Create a file called `.env` at the root level to specify the API base url:

```
REACT_APP_API_PRODUCTION=http://***.***.**.**:****
REACT_APP_API_DEV=http://localhost:3030
```

## Commands

### Development

Use `REACT_API_API` command line parameter to specify which API to connect to.
To connect to the `dev` API:

```
REACT_APP_API=dev npm start
```

To connect to the `production` API:

```
REACT_APP_API=production npm start
```

### Production deploy

STEP 1: generate the `build` folder

```
npm run build
```

STEP 2: push the build folder to `surge.sh`

```
npm run deploy
```
