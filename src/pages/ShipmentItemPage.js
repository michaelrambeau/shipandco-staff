import React from 'react'

import FetchItem from '../containers/FetchItem'
import ShipmentItem from '../components/ShipmentItem'

const ShipmentItemPage = ({ match }) => {
  const { id } = match.params
  return (
    <FetchItem endPoint="shipments" id={id}>
      {({ item, history }) => (
        <section className="section">
          <div className="container">
            <ShipmentItem shipment={item} history={history} />
          </div>
        </section>
      )}
    </FetchItem>
  )
}

export default ShipmentItemPage
