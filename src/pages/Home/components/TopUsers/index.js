import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import numeral from 'numeral'

import TimeAgo from '../../../../components/utils/TimeAgo'
import groupUsers from './groupUsers'
import goToUser from '../../../../components/UserList/goToUser'

const TopUsers = ({ users, history }) => {
  const groups = users ? groupUsers(users).slice(0, 10) : []
  return (
    <div className="box">
      <div className="title is-4">Top 10 Customers</div>
      <div className="subtitle is-6">By number of shipment</div>
      <hr />
      <UserGroupList groups={groups} history={history} />
      <div style={{ textAlign: 'center' }}>
        <Link to="/users">View all users</Link>
      </div>
    </div>
  )
}

const UserGroupList = ({ groups, history }) => (
  <div>
    {groups.map((group, i) => (
      <UserGroup
        key={group.key}
        group={group}
        ranking={i + 1}
        history={history}
      />
    ))}
  </div>
)

const UserGroup = ({ group, ranking, history }) => (
  <div>
    <div
      style={{ display: 'flex', marginBottom: '.2rem', alignItems: 'center' }}
    >
      <span className="tag is-primary" style={{ marginRight: '.3rem' }}>
        #{ranking}
      </span>
      <span style={{ flexGrow: '1', fontSize: '1.2rem' }}>
        {!!group.name && group.name}
      </span>
      {group.members.length > 1 && (
        <span className="empty">
          {`Total: ${numeral(group.total).format('0,0')}`}
        </span>
      )}
    </div>
    <UserTable users={group.members} history={history} />
  </div>
)

const UserTable = ({ users, history }) => (
  <table className={`table is-bordered clickable`}>
    <tbody>
      {users.map((user, i) => (
        <UserTableRow
          user={user}
          key={user._id}
          ranking={i + 1}
          history={history}
        />
      ))}
    </tbody>
  </table>
)

const UserTableRow = ({ user, ranking, history }) => {
  const onClickRow = goToUser(history)
  return (
    <tr onClick={onClickRow(user)}>
      <td>
        {user.email}
        <div className="light-text">
          Last shipment: <TimeAgo datetime={user.lastShipment} />
        </div>
      </td>
      <td width="80" style={{ textAlign: 'right' }}>
        {numeral(user.count).format('0,0')}
      </td>
    </tr>
  )
}

export default withRouter(TopUsers)
