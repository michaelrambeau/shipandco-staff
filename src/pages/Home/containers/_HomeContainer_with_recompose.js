import React from 'react'
import {
  compose,
  lifecycle,
  branch,
  renderComponent
  // withState
} from 'recompose'

import HomeView from '../components/HomeView'
import Loading from '../../../components/utils/Loading'
import { apiFetch } from '../../../api'

const apiFetchDashboard = () =>
  apiFetch({
    endPoint: 'dashboard'
  })

const withDashboardData = lifecycle({
  state: {
    loading: true
  },
  fetchData() {
    apiFetchDashboard().then(data => this.setState({ loading: false, ...data }))
  },
  componentDidMount() {
    const { query } = this.props
    this.fetchData({ query })
  },
  componentWillReceiveProps(nextProps) {
    this.fetchData()
  }
})

const isLoading = ({ loading }) => loading

const withSpinnerWhileLoading = branch(isLoading, renderComponent(Loading))

const enhance = compose(withDashboardData, withSpinnerWhileLoading)

export default enhance(props => {
  console.log('REnder', props)

  return <HomeView {...props} />
})
