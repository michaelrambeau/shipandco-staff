import React, { Component } from 'react'

import HomeView from '../components/HomeView'
import Loading from '../../../components/utils/Loading'
import { apiFetch } from '../../../api'

const apiFetchDashboard = () =>
  apiFetch({
    endPoint: 'dashboard'
  })

class HomeContainer extends Component {
  state = {
    loading: true
  }
  constructor() {
    super()
    this.onRefresh = this.onRefresh.bind(this)
  }
  fetchData() {
    this.setState({ loading: true })
    apiFetchDashboard().then(data => this.setState({ loading: false, ...data }))
  }
  onRefresh() {
    this.fetchData()
  }
  componentDidMount() {
    const { query } = this.props
    this.fetchData({ query })
  }
  componentWillReceiveProps(nextProps) {
    this.fetchData()
  }
  render() {
    const props = this.state
    const { loading } = props
    if (loading) return <Loading />
    return <HomeView {...props} onRefresh={this.onRefresh} />
  }
}

export default HomeContainer
