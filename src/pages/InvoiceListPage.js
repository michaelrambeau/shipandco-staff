import React from 'react'

import FetchList from '../containers/FetchList'
import ListWithPagination from '../containers/ListWithPagination'
import PaginationContainer from '../containers/PaginationContainer'

import InvoiceList from '../components/InvoiceList'

const InvoiceListPage = () => {
  const endPoint = 'payments'
  const options = { showUser: true }
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit }) => {
        const query = { $limit, $skip }
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">Latest invoices</h2>
                  <ListWithPagination
                    pageIndex={pageIndex}
                    total={total}
                    skip={$skip}
                    limit={$limit}
                    endPoint={endPoint}
                  >
                    {() => (
                      <InvoiceList
                        invoices={data}
                        count={total}
                        options={options}
                      />
                    )}
                  </ListWithPagination>
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}

export default InvoiceListPage
