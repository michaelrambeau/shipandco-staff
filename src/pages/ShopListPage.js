import React from 'react'

import FetchList from '../containers/FetchList'
import ListWithPagination from '../containers/ListWithPagination'
import PaginationContainer from '../containers/PaginationContainer'
import ShopIcon from '../components/utils/ShopIcon'
import allShops from '../data/shops'

import ShopList from '../components/ShopList'
import ShopTypeSelector from '../components/utils/ShopTypeSelector'

const Title = ({ shop }) => {
  if (!shop) return <span>All shops</span>
  const shopItem = allShops.find(item => item.value === shop)
  const text = shopItem ? shopItem.text : shop
  return (
    <span>
      <ShopIcon type={shop} />
      {text} shops
    </span>
  )
}

const ShopListPage = () => {
  const endPoint = 'shops'
  const $sort = '-meta.created_at'
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit, shop }) => {
        const baseQuery = { $limit, $skip, $sort }
        const query = shop ? { ...baseQuery, 'meta.type': shop } : baseQuery
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">
                    <Title shop={shop} /> <span>({total})</span>
                  </h2>
                  <ShopTypeSelector baseUrl={'/shops?'} current={shop} />
                  <ListWithPagination
                    pageIndex={pageIndex}
                    total={total}
                    skip={$skip}
                    limit={$limit}
                    endPoint={endPoint}
                    query={query}
                  >
                    {() => <ShopList shops={data} count={total} />}
                  </ListWithPagination>
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}

export default ShopListPage
