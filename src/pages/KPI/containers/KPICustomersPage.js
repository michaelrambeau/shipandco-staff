import React from 'react'
import {
  compose,
  lifecycle,
  branch,
  renderComponent
  // withState
} from 'recompose'

import KPIView from '../components/KPIUsersView'
import Loading from '../../../components/utils/Loading'
import { apiFetch } from '../../../api'
import withQueryFilters from './withQueryFilters'

const apiFetchKPI = ({ query }) =>
  apiFetch({
    endPoint: 'kpi/customers',
    query
  })

const withKPIData = lifecycle({
  state: {
    loading: true
  },
  fetchData({ query }) {
    apiFetchKPI({ query }).then(data =>
      this.setState({ loading: false, ...data })
    )
  },
  componentDidMount() {
    const { query } = this.props
    this.fetchData({ query })
  },
  componentWillReceiveProps(nextProps) {
    const { query } = nextProps
    this.fetchData({ query })
  }
})

const isLoading = ({ loading }) => loading

const withSpinnerWhileLoading = branch(isLoading, renderComponent(Loading))

const enhance = compose(withKPIData, withSpinnerWhileLoading)

export default withQueryFilters(
  enhance(({ query, results, onChangeFilter }) => (
    <KPIView data={results} query={query} onChangeFilter={onChangeFilter} />
  ))
)
