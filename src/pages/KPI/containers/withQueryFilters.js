import React from 'react'

const withQueryFilters = Wrapped =>
  class Wrapper extends React.Component {
    constructor(props) {
      super(props)
      this.onChangeFilter = this.onChangeFilter.bind(this)
      this.state = {
        query: {
          shop: '*',
          carrier: '*',
          user: '*'
        }
      }
    }
    onChangeFilter({ key, value }) {
      const query = { ...this.state.query, [key]: value }
      this.setState({ query })
    }
    render() {
      const { query } = this.state
      return (
        <Wrapped
          {...this.props}
          query={query}
          onChangeFilter={this.onChangeFilter}
        />
      )
    }
  }

export default withQueryFilters

// const withFilters = withState('query', 'setQuery', {
//   shop: '*',
//   carrier: '*',
//   user: '*'
// })
