import React from 'react'

import FetchItem from '../containers/FetchItem'
import OrderItem from '../components/OrderItem'

const OrderItemPage = ({ match }) => {
  const { id } = match.params
  return (
    <FetchItem endPoint="orders" id={id}>
      {({ item, history }) => (
        <section className="section">
          <div className="container">
            <OrderItem order={item} history={history} />
          </div>
        </section>
      )}
    </FetchItem>
  )
}

export default OrderItemPage
