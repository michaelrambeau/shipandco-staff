import React from 'react'

import FetchItem from '../containers/FetchItem'
import ShopItem from '../components/ShopItem'

const ShopItemPage = ({ match }) => {
  console.log({ match })
  const { id } = match.params
  return (
    <FetchItem endPoint="shops" id={id}>
      {({ item, history }) => (
        <section className="section">
          <div className="container">
            <ShopItem shop={item} history={history} />
          </div>
        </section>
      )}
    </FetchItem>
  )
}

export default ShopItemPage
