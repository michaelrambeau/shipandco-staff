import React from 'react'

import FetchItem from '../containers/FetchItem'
import UserItem from '../components/UserItem'

const CustomerItemPage = ({ match }) => {
  const { id } = match.params
  return (
    <FetchItem endPoint="customers" id={id}>
      {({ item }) => (
        <section className="section">
          <div className="container">
            <UserItem user={item} />
          </div>
        </section>
      )}
    </FetchItem>
  )
}

export default CustomerItemPage
