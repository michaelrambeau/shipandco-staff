import React from 'react'

import FetchList from '../containers/FetchList'
import ListWithPagination from '../containers/ListWithPagination'
import PaginationContainer from '../containers/PaginationContainer'

import CarrierSelector from '../components/utils/CarrierSelector'
import ShipmentList from '../components/ShipmentList'
import CarrierIcon from '../components/utils/CarrierIcon'

import carriers from '../data/carriers'

const Title = ({ carrier }) => {
  if (!carrier) return <span>All shipments</span>
  const carrierItem = carriers[carrier]
  const text = carrierItem ? carrierItem.shortName || carrierItem.name : carrier
  return (
    <span>
      <CarrierIcon type={carrier} /> {text} shipments
    </span>
  )
}

const ShipmentListPage = () => {
  const endPoint = 'shipments'
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit, carrier }) => {
        const baseQuery = { $limit, $skip }
        const query = carrier
          ? { ...baseQuery, 'delivery.carrier': carrier }
          : baseQuery
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">
                    <Title carrier={carrier} /> <span>({total})</span>
                  </h2>
                  <CarrierSelector baseUrl={'/shipments?'} current={carrier} />
                  <ListWithPagination
                    pageIndex={pageIndex}
                    total={total}
                    skip={$skip}
                    limit={$limit}
                    endPoint={endPoint}
                    query={query}
                  >
                    {() => <ShipmentList shipments={data} count={total} />}
                  </ListWithPagination>
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}

export default ShipmentListPage
