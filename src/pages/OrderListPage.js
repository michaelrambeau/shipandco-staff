import React from 'react'
import numeral from 'numeral'

import FetchList from '../containers/FetchList'
import ListWithPagination from '../containers/ListWithPagination'
import PaginationContainer from '../containers/PaginationContainer'
import ShopIcon from '../components/utils/ShopIcon'
import allShops from '../data/shops'

import ShopTypeSelector from '../components/utils/ShopTypeSelector'
import OrderList from '../components/OrderList'

const Title = ({ shop }) => {
  if (!shop) return <span>Orders from all shops</span>
  const shopItem = allShops.find(item => item.value === shop)
  const text = shopItem ? shopItem.text : shop
  return (
    <span>
      <ShopIcon type={shop} />
      Orders from {text} shops
    </span>
  )
}

const OrderListPage = () => {
  const endPoint = 'orders'

  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit, shop }) => {
        const baseQuery = { $limit, $skip }
        const query = shop ? { ...baseQuery, 'meta.type': shop } : baseQuery
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">
                    <Title shop={shop} />{' '}
                    <span>({numeral(total).format('0,0')})</span>
                  </h2>
                  <ShopTypeSelector baseUrl={'/orders?'} current={shop} />
                  <ListWithPagination
                    pageIndex={pageIndex}
                    total={total}
                    skip={$skip}
                    limit={$limit}
                    query={shop ? { 'meta.type': shop } : {}}
                  >
                    {() => <OrderList orders={data} count={total} />}
                  </ListWithPagination>
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}

export default OrderListPage
