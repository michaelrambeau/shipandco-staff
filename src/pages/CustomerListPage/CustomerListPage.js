import React from 'react'

import FetchList from '../../containers/FetchList'

import UserList from '../../components/UserList'
import CustomerListWithSearchModule from './CustomerListWithSearchModule'

const CustomerListPage = () => (
  <FetchList endPoint={'customers'}>
    {({ total, data }) => (
      <section className="section">
        <div className="container">
          <h2 className="title is-4">All customers ({total})</h2>
          <CustomerListWithSearchModule users={data}>
            {({ filteredItems }) => (
              <UserList users={filteredItems} count={total} />
            )}
          </CustomerListWithSearchModule>
        </div>
      </section>
    )}
  </FetchList>
)
export default CustomerListPage
