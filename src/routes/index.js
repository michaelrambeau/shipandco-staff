import React from 'react'
import { BrowserRouter as Router, Route as PublicRoute } from 'react-router-dom'

import Route from '../auth/PrivateRoute'
import Layout from '../Layout'
import auth from '../auth/auth'

// Public pages
import Login from '../pages/Login'

// Private pages
import Home from '../pages/Home'
import KPIShipments from '../pages/KPI/containers/KPIShipmentsPage'
import KPICustomers from '../pages/KPI/containers/KPICustomersPage'
import ShipmentListPage from '../pages/ShipmentListPage'
import ShipmentItemPage from '../pages/ShipmentItemPage'
import OrderListPage from '../pages/OrderListPage'
import OrderItemPage from '../pages/OrderItemPage'
import CustomerListPage from '../pages/CustomerListPage'
import CustomerItemPage from '../pages/CustomerItemPage'
import ShopListPage from '../pages/ShopListPage'
import ShopItemPage from '../pages/ShopItemPage'
import InvoiceListPage from '../pages/InvoiceListPage'

class Routes extends React.Component {
  constructor() {
    super()
    this.state = {
      auth: {
        pending: true,
        isAuthenticated: false,
      },
    }
    this.checkAuth = this.checkAuth.bind(this)
    this.handleLogOut = this.handleLogOut.bind(this)
  }
  checkAuth() {
    auth.checkAuth().then(res => {
      if (!res)
        return this.setState({
          auth: {
            pending: false,
            isAuthenticated: false,
          },
        })
      return this.setState({
        auth: {
          pending: false,
          isAuthenticated: true,
        },
      })
    })
  }

  handleLogOut() {
    auth
      .logOut()
      .then(() =>
        this.setState({
          auth: {
            pending: false,
            isAuthenticated: false,
          },
        })
      )
      .catch(err => console.log(err))
  }

  componentDidMount() {
    this.checkAuth()
  }

  render() {
    const auth = this.state.auth
    const handleLogOut = this.handleLogOut
    return (
      <Router>
        <Layout auth={auth} handleLogOut={handleLogOut}>
          <Route exact path="/" component={Home} auth={auth} />
          <PublicRoute exact path="/login" component={Login} auth={auth} />
          <Route exact path="/kpi" component={KPIShipments} auth={auth} />
          <Route
            exact
            path="/kpi/customers"
            component={KPICustomers}
            auth={auth}
          />
          <Route
            exact
            path="/shipments"
            component={ShipmentListPage}
            auth={auth}
          />
          <Route
            exact
            path="/shipments/:id"
            component={ShipmentItemPage}
            auth={auth}
          />
          <Route exact path="/orders" component={OrderListPage} auth={auth} />
          <Route
            exact
            path="/orders/:id"
            component={OrderItemPage}
            auth={auth}
          />
          <Route exact path="/users" component={CustomerListPage} auth={auth} />
          <Route path="/users/:id" component={CustomerItemPage} auth={auth} />
          <Route exact path="/shops" component={ShopListPage} auth={auth} />
          <Route path="/shops/:id" component={ShopItemPage} auth={auth} />
          <Route path="/invoices" component={InvoiceListPage} auth={auth} />
        </Layout>
      </Router>
    )
  }
}

export default Routes
