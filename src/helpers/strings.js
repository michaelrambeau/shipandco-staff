export function limitString(chars, string, extend = '...') {
  if (string.length > chars) {
    return string.substring(0, chars - extend.length) + extend
  }
  return string
}
