import get from 'lodash.get'

export function getLabelPath(shipment) {
  const { _id, meta } = shipment
  const { created_at, user_id } = meta
  const d = new Date(created_at)
  const month = d.getMonth()
  const year = d.getFullYear()
  const m = `${month + 1 < 10 ? '0' : ''}${month + 1}`
  return `labels/${year}${m}/${user_id}/${_id}`
}

export function getLabelExtension(shipment) {
  const labelType = get(shipment, 'label.type')
  switch (labelType) {
    case 'pdf':
      return 'pdf'
    default:
      return 'txt'
  }
}

export function getLabelUrl(shipment) {
  const bucketName = get(shipment, 'label.bucket')
  const path = getLabelPath(shipment)
  const extension = getLabelExtension(shipment)
  return `https://storage.googleapis.com/${bucketName}/${path}.${extension}`
}
