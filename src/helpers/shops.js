export function isSyncLate(shop) {
  const { sync } = shop
  const syncDate = sync && sync.synced_at
  const now = new Date()
  const diffMs = now - new Date(syncDate)
  const diffMinutes = diffMs / 1000 / 60
  return diffMinutes > 15
}
