export function getFullname(address) {
  const { full_name, last_name, first_name } = address
  return full_name ? full_name : `${first_name} ${last_name}`
}
