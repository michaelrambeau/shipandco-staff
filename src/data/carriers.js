const carriers = {
  japanpost: {
    name: 'JapanPost (International)',
    shortName: 'JPost'
  },
  dhl: {
    name: 'DHL'
  },
  ups: {
    name: 'UPS'
  },
  fedex: {
    name: 'FedEx'
  },
  yuupack: {
    name: 'Yuupack'
  },
  yuupacket: {
    name: 'Yuupacket'
  },
  yamato: {
    name: 'Yamato'
  },
  sagawa: {
    name: 'Sagawa'
  }
}

export default carriers
