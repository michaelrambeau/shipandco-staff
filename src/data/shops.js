const shops = [
  { value: 'amazon', text: 'Amazon' },
  { value: 'base', text: 'Base' },
  { value: 'ebay', text: 'eBay' },
  { value: 'magento', text: 'Magento' },
  { value: 'nextengine', text: 'NextEngine' },
  { value: 'prestashop', text: 'Prestashop' },
  { value: 'rakuten', text: 'Rakuten' },
  { value: 'shopify', text: 'Shopify' },
  { value: 'stripe', text: 'Stripe' },
]
export default shops
