import React from 'react'
import {
  // BrowserRouter as Router,
  Route
  // Link,
  // Redirect,
  // withRouter
} from 'react-router-dom'
import Login from '../pages/Login'

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (auth.pending) return <div>Authentication...</div>
        return auth.isAuthenticated ? (
          <Component {...props} />
        ) : (
          // <Redirect
          //   to={{
          //     pathname: '/login',
          //     state: { from: props.location },
          //   }}
          // />
          <Login {...props} />
        )
      }}
    />
  )
}

export default PrivateRoute
