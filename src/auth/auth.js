import feathers from 'feathers-client';

import config from '../config';

const API_BASE_URL = config.apiBaseUrl;

const app = feathers();

function initClient() {
  const host = API_BASE_URL;
  console.info('Launch the auth client', host);
  app
    .configure(feathers.rest(host).fetch(window.fetch))
    .configure(feathers.hooks())
    .configure(feathers.authentication({ storage: window.localStorage }));
  // app
  //   .authenticate()
  //   .then(res => console.log('Auth OK!', res))
  //   .catch(err => console.error(err))
}

initClient();

const auth = {
  checkAuth() {
    return app
      .authenticate()
      .then(res => {
        console.log('Authentication OK!', res);
        return res;
      })
      .catch(err => console.error(err));
  },
  logOut() {
    return app
      .logout()
      .then(res => console.log('Successfully logged out!'))
      .catch(err => console.error(err));
  },
};

export default auth;
