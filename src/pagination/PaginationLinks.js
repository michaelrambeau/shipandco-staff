import React from 'react'

import Pagination from './Pagination'

const PaginationLinks = ({ url, pageNumber, total, query, pageSize }) => (
  <Pagination
    currentPage={pageNumber}
    query={query}
    total={total}
    limit={10}
    pageSize={pageSize}
    url={url}
    singular={'item'}
    plural={'items'}
    style={{ paddingBottom: '1rem' }}
  />
)

export default PaginationLinks
