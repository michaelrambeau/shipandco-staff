# About the pagination

## Option 1: using a `withPagination()` HoF to wrap add pagination buttons

```js
const OrderListWithPagination = withPagination(OrderList)

const OrderListPage = () => {
  const endPoint = 'orders'
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit }) => {
        const query = { $limit, $skip }
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">All open orders ({total})</h2>
                  <OrderListWithPagination
                    orders={data}
                    count={total}
                    pageIndex={pageIndex}
                    skip={$skip}
                    limit={$limit}
                    total={total}
                    endPoint={endPoint}
                  />
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}
```

## Option 2: using a component `<ListWithPagination>` that accepts a function as child

```js
const ShipmentListPage = () => {
  const endPoint = 'shipments'
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit }) => {
        const query = { $limit, $skip }
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <section className="section">
                <div className="container">
                  <h2 className="title is-4">All shipments ({total})</h2>
                  <ListWithPagination
                    pageIndex={pageIndex}
                    total={total}
                    skip={$skip}
                    limit={$limit}
                    endPoint={endPoint}
                  >
                    {() => <ShipmentList shipments={data} count={total} />}
                  </ListWithPagination>
                </div>
              </section>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}
```
