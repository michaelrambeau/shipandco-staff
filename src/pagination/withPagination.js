import React from 'react'
import PaginationLinks from './PaginationLinks'

const withPagination = WrappedComponent => props => {
  const { endPoint, pageIndex, total, limit } = props
  return (
    <div>
      <PaginationLinks
        query={{}}
        pageNumber={pageIndex + 1}
        pageSize={limit}
        url={`/${endPoint}`}
        total={total}
      />
      <WrappedComponent {...props} />
      <PaginationLinks
        query={{}}
        pageNumber={pageIndex + 1}
        pageSize={limit}
        url={`/${endPoint}`}
        total={total}
      />
    </div>
  )
}

export default withPagination
