import React, { Component } from 'react'
import { withRouter } from 'react-router'

import Loading from '../components/utils/Loading'
import { apiFetch } from '../api'

const apiFetchSingleItem = ({ endPoint, id }) =>
  // Promise.resolve({ total: 2, skip: 0, limit: 2, data: ['A', 'B'] })
  apiFetch({
    endPoint: `${endPoint}/${id}`
  })

class FetchItem extends Component {
  state = {
    loading: true
  }
  constructor() {
    super()
    this.onRefresh = this.onRefresh.bind(this)
  }
  fetchData() {
    const { endPoint, id } = this.props
    this.setState({ loading: true })
    apiFetchSingleItem({ endPoint, id }).then(payload =>
      this.setState({ loading: false, item: payload })
    )
  }
  onRefresh() {
    this.fetchData()
  }
  componentDidMount() {
    const { query } = this.props
    this.fetchData({ query })
  }
  componentWillReceiveProps(nextProps) {
    this.fetchData()
  }
  render() {
    const { children, history } = this.props
    const { loading } = this.state
    if (loading) return <Loading />
    return children({ ...this.state, history })
  }
}

export default withRouter(FetchItem)
