import React, { Component } from 'react'
import { withRouter } from 'react-router'

import Loading from '../components/utils/Loading'
import { apiFetch } from '../api'

const apiFetchListItems = ({ endPoint, query }) =>
  apiFetch({
    endPoint,
    query
  })

class FetchList extends Component {
  state = {
    loading: true
  }
  constructor() {
    super()
    this.onRefresh = this.onRefresh.bind(this)
  }
  fetchData(props) {
    const { endPoint, query } = props
    this.setState({ loading: true })
    apiFetchListItems({ endPoint, query }).then(payload =>
      this.setState({ loading: false, ...payload })
    )
  }
  onRefresh() {
    this.fetchData()
  }
  componentDidMount() {
    this.fetchData(this.props)
  }
  componentWillReceiveProps(nextProps) {
    this.fetchData(nextProps)
  }
  render() {
    const { children, history } = this.props
    const { loading } = this.state
    if (loading) return <Loading />
    return <div>{children({ ...this.state, history })}</div>
  }
}

export default withRouter(FetchList)
