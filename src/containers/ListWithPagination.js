import React, { Component } from 'react'

import PaginationLinks from '../pagination/PaginationLinks'

// Component used to render a list of items with pagination links above and below the list
class ListWithPagination extends Component {
  render() {
    const { children, endPoint, pageIndex, query, total, limit } = this.props
    return (
      <div>
        <PaginationLinks
          query={query}
          pageNumber={pageIndex + 1}
          pageSize={limit}
          url={`/${endPoint}`}
          total={total}
        />
        {/* Call the child function, without any parameter, for now */}
        {children()}
        <PaginationLinks
          query={query}
          pageNumber={pageIndex + 1}
          pageSize={limit}
          url={`/${endPoint}`}
          total={total}
        />
      </div>
    )
  }
}

export default ListWithPagination
