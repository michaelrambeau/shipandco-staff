import { withRouter } from 'react-router'
import { parse } from 'query-string'

// Top level pagination container, used to read URL `page` parameters
// and pass `$skip` and `$limit` to the child function
const PaginationContainer = ({ children, location, match }) => {
  const queryStringParameters = parse(location.search)
  const { page } = queryStringParameters
  const shop = queryStringParameters['meta.type']
  const carrier = queryStringParameters['delivery.carrier']
  const pageNumber = page && !isNaN(page) ? parseInt(page, 0) : 1
  const pageIndex = pageNumber - 1
  const pageSize = 50
  const $skip = page ? pageIndex * pageSize : 0
  const $limit = pageSize
  return children({ pageIndex, $skip, $limit, carrier, shop })
}

export default withRouter(PaginationContainer)
