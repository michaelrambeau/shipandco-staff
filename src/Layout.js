import React from 'react'

import Header from './components/Header/Header'
import TimeAgo from './components/utils/TimeAgo'

const updatedAt = new Date(process.env.BUILD_DATE)

class Layout extends React.Component {
  render() {
    const auth = this.props.auth
    const handleLogOut = this.props.handleLogOut
    return (
      <div
        style={{ display: 'flex', minHeight: '100vh', flexDirection: 'column' }}
      >
        <Header auth={auth} handleLogOut={handleLogOut} />
        <div style={{ flex: 1 }}>{this.props.children}</div>
        <footer className="footer">
          <div className="container">
            <p>
              <span>SHIP&CO Dashboard</span>
              <span className="tag is-primary" style={{ marginLeft: '.5rem' }}>
                Staff only!
              </span>
            </p>
            <p>
              Last update: <TimeAgo datetime={updatedAt} />
            </p>
          </div>
        </footer>
      </div>
    )
  }
}

export default Layout
