import config from './config';

const API_BASE_URL = config.apiBaseUrl;

function request(url, options) {
  return fetch(url, options).then(response => {
    if (response.status >= 400 && response.status !== 404) {
      throw new Error(`Bad response (${response.status}) from server ${url}`);
    }
    if (response.status === 404) {
      window.location = '/';
    }
    return response.json();
  });
}

function requestWithToken(token) {
  return function(url, options = {}) {
    const headers = {
      ...options.headers,
      authorization: token,
    };
    const reqOptions = {
      ...options,
      headers,
    };
    return request(url, reqOptions);
  };
}

export function apiFetchData({ token, key, query, type }) {
  const mapping = {
    shipments: 'kpi/shipments',
    customers: 'kpi/customers',
  };
  const endPoint = (type && mapping[type]) || key;
  const url = `${API_BASE_URL}/${endPoint}`;
  const qs = Object.keys(query)
    .map(k => `${k}=${query[k]}`)
    .join('&');
  const fullUrl = qs ? `${url}?${qs}` : url;
  return requestWithToken(token)(fullUrl);
}

export function getToken() {
  return localStorage.getItem('feathers-jwt');
}

export function apiFetch({ endPoint, query }) {
  console.log('API Fetch!', endPoint, query);

  const url = `${API_BASE_URL}/${endPoint}`;
  const qs =
    query &&
    Object.keys(query)
      .map(k => `${k}=${query[k]}`)
      .join('&');
  const fullUrl = qs ? `${url}?${qs}` : url;
  const token = getToken();
  return requestWithToken(token)(fullUrl);
}
