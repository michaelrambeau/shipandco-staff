// Reading the context, checking the command line parameter to start the app,
// using an environment variable to specify which API to connect to.
// `REACT_APP_API=production npm start` to connect the app to the production API
const apiContext = process.env.REACT_APP_API

const apiEnvKey = apiContext ? apiContext.toUpperCase() : 'DEV'
const envFileKey = `REACT_APP_API_${apiEnvKey.toUpperCase()}`
console.info(`> Starting the app connecting to "${apiEnvKey}" API`)

const url = process.env[envFileKey]
if (!url)
  throw new Error(`Ship&Co Staff ERROR: no env variable found ${envFileKey}`)

// Export variables
// export const apiBaseUrl = url
export default { apiBaseUrl: url }
