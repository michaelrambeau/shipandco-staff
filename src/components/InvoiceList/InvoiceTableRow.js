import React from 'react'
import tinytime from 'tinytime'

import Amount from '../utils/Amount'
import { getFullname } from '../../helpers/addresses'

const templates = {
  full: tinytime('{YYYY}/{Mo}/{DD} {H}:{mm}', {
    padMonth: true,
    padDays: true,
  }),
  compact: tinytime('{YYYY}/{Mo}/{DD}', {
    padMonth: true,
    padDays: true,
  }),
}

const ContactName = ({ user }) => {
  const { contact } = user
  if (!contact) return <span className="empty">(no contact)</span>
  const { email } = contact
  return (
    <div>
      {getFullname(contact)}
      {email !== user.emails[0].address && <div>{email}</div>}
    </div>
  )
}

const goToUser = history => user => {
  return () => history.push(`/users/${user._id}/billing`)
}

const InvoiceTableRow = ({ invoice, history, options = {} }) => {
  const onClickRow = goToUser(history)
  const { user } = invoice
  return (
    <tr onClick={onClickRow(user)}>
      {options.showUser && (
        <td>
          {user.emails[0].address}
          <br />
          <ContactName user={user} />
          {user.billing && (
            <div style={{ color: '#209cee' }}>{user.billing.customer_id}</div>
          )}
        </td>
      )}
      <td>{templates.full.render(new Date(invoice.date))}</td>
      <td>
        <Amount value={invoice.amount} currency={'JPY'} />
      </td>
      <td>
        <ul>
          {invoice.items
            .filter(item => !!item.description) // remove items whose description is null
            .map(item => <InvoiceItem key={item.id} item={item} />)}
          {invoice.itemsCount > 10 && (
            <li>{invoice.itemsCount - 10} more items...</li>
          )}
        </ul>
      </td>
    </tr>
  )
}

const InvoiceItem = ({ item }) => (
  <li>
    {item.description} = <Amount value={item.amount} currency="jpy" />
  </li>
)

export default InvoiceTableRow
