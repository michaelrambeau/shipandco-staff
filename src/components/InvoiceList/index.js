import React from 'react'
import { withRouter } from 'react-router-dom'

import InvoiceTableRow from './InvoiceTableRow'

const InvoiceList = ({ invoices, options = {}, history }) => {
  return (
    <table className="table is-striped clickable">
      <thead>
        <tr>
          {options.showUser && <td>Customer</td>}
          <td>Date</td>
          <td>Amount</td>
          <td>Items</td>
        </tr>
      </thead>
      <tbody>
        {invoices.map(invoice => (
          <InvoiceTableRow
            invoice={invoice}
            key={invoice.id}
            options={options}
            history={history}
          />
        ))}
      </tbody>
    </table>
  )
}

export default withRouter(InvoiceList)
