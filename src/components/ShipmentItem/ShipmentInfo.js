import React from 'react'
import get from 'lodash.get'
import tinytime from 'tinytime'

import CarrierIcon from '../utils/CarrierIcon'
import Amount from '../utils/Amount'
import TimeAgo from '../utils/TimeAgo'
import LabelArea from './LabelArea'

const methodName = data => {
  const { carrier, method } = data
  if (carrier === 'japanpost') return method.toUpperCase()
  if (carrier === 'yamato') return 'Yamato (C2)'
  return method
}

const templates = {
  full: tinytime('{YYYY}/{Mo}/{DD} {H}:{mm}', {
    padMonth: true,
    padDays: true
  })
}

const Info = ({ data, shipment }) => {
  const { meta, delivery } = shipment
  const date = meta.created_at
  const trackingNumber = get(delivery, 'tracking_numbers[0]') || ''
  return (
    <div className="card is-fullwidth">
      <div className="card-content media">
        <div className="media-left">
          <CarrierIcon type={delivery.carrier} size={48} />
        </div>
        <div className="media-content">
          <div className="content">
            <div>
              Created at {templates.full.render(new Date(date))} (<TimeAgo
                datetime={date}
              />)
            </div>
            <div>Tracking Number: {trackingNumber || 'N/A'}</div>
            {delivery.method && <div>{methodName(delivery)}</div>}
            {!!delivery.amount && (
              <div>
                <Amount value={delivery.amount} currency={delivery.currency} />
              </div>
            )}
            {meta && <Billing meta={meta} />}
          </div>
        </div>
        <div className="media-right">
          {shipment && <LabelArea shipment={shipment} />}
        </div>
      </div>
    </div>
  )
}

const Invoiced = ({ invoiced_at }) => (
  <div>
    <span className="fa fa-check-square light-text" /> Invoiced at{' '}
    {templates.full.render(new Date(invoiced_at))} (<TimeAgo
      datetime={invoiced_at}
    />)
  </div>
)

const NotInvoicedYet = ({ invoiced_at }) => (
  <div className="empty">Not invoiced yet</div>
)

const Free = () => (
  <div>
    <span className="fa fa-gift light-text" /> This is a FREE label!
  </div>
)

const Billing = ({ meta }) => {
  const { free, invoiced_at } = meta
  if (free) return <Free />
  return invoiced_at ? (
    <Invoiced invoiced_at={invoiced_at} />
  ) : (
    <NotInvoicedYet />
  )
}

export default Info
