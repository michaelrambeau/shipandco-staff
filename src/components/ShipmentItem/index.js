import React from 'react'

import OrderShipmentHeader from '../utils/OrderShipmentHeader'
import ShipmentInfo from './ShipmentInfo'
import AddressFrom from '../OrderItem/AddressFrom'
import ParcelList from '../OrderItem/ParcelList'
import ProductList from '../OrderItem/ProductList'
import AddressTo from '../OrderItem/AddressTo'

const Shipment = ({ shipment }) => {
  const { products, parcels } = shipment
  return (
    <div>
      <OrderShipmentHeader
        model="shipments"
        record={shipment}
        title={`Shipment ${shipment && shipment.meta.identifier}`}
      />
      <ShipmentInfo data={shipment.shipment_infos} shipment={shipment} />
      <br />
      {products.length > 0 && (
        <div className="card is-fullwidth" style={{ marginBottom: '2rem' }}>
          <div className="card-header">
            <div className="card-header-title">Products</div>
          </div>
          <div className="card-content">
            <ProductList
              products={products}
              currency={shipment.meta.currency}
            />
          </div>
        </div>
      )}
      {parcels &&
        parcels.length > 0 && (
          <div className="card is-fullwidth" style={{ marginBottom: '2rem' }}>
            <div className="card-header">
              <div className="card-header-title">Parcels</div>
            </div>
            <div className="card-content">
              <ParcelList parcels={parcels} />
            </div>
          </div>
        )}
      <div className="columns">
        {shipment.from_address && (
          <div className="column is-half">
            <div className="card is-fullwidth">
              <div className="card-header">
                <div className="card-header-title">Sender Address</div>
              </div>
              <div className="card-content">
                <AddressFrom address={shipment.from_address} />
              </div>
            </div>
          </div>
        )}
        <div className="column is-half">
          <div className="card is-fullwidth">
            <div className="card-header">
              <div className="card-header-title">Shipping Address</div>
            </div>
            <div className="card-content">
              <AddressTo address={shipment.to_address} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Shipment
