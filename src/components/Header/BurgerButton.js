import React from 'react'
import classNames from 'classnames'

const BurgerButton = ({ isActive, onToggle }) => (
  <div
    onClick={onToggle}
    className={classNames('my-own-class', 'navbar-burger', {
      'is-active': isActive,
    })}
  >
    <span />
    <span />
    <span />
  </div>
)

export default BurgerButton
