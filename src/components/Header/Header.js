import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { withState } from 'recompose'

import logo from './logo.svg'
import Menu from './Menu'
import BurgerButton from './BurgerButton'

import './Header.css'

const Header = ({
  auth,
  handleLogOut,
  history,
  location,
  match,
  isActive,
  setIsActive,
}) => {
  return (
    <nav className="navbar has-shadow">
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <img src={logo} alt="logo" height="28" style={{ height: 28 }} />
          </Link>
          <BurgerButton
            isActive={isActive}
            onToggle={() => setIsActive(!isActive)}
          />
        </div>
        <Menu
          isActive={isActive}
          location={location}
          auth={auth}
          handleLogOut={handleLogOut}
        />
      </div>
    </nav>
  )
}

const enhance = withState('isActive', 'setIsActive', false)

export default withRouter(enhance(Header))
