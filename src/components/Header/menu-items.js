const menuItems = [
  {
    path: '/',
    text: 'Dashboard',
    isIndex: true,
    icon: 'home',
  },
  {
    path: '/users',
    text: 'Customers',
  },
  {
    path: '/addresses',
    text: 'Address Book',
    disabled: true,
  },
  {
    path: '/shops',
    text: 'Shops',
  },
  {
    path: '/orders',
    text: 'Orders',
  },
  {
    path: '/shipments',
    text: 'Shipments',
  },
  {
    path: '/kpi',
    text: 'KPI',
  },
  {
    path: '/invoices',
    text: 'Invoices',
  },
].filter(item => !item.disabled)

export default menuItems
