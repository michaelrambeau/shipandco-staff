import React from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'

import items from './menu-items'

const Item = props => {
  const { pathName, navItem } = props
  const { path, text, icon } = navItem
  const isActive = pathName === path
  return (
    <Link
      to={path}
      className={`navbar-item is-tab${isActive ? ' is-active' : ''}`}
    >
      {icon ? <span className={`fa fa-${icon}`} /> : <span>{text}</span>}
    </Link>
  )
}

const Menu = ({ location, auth, handleLogOut, isActive }) => (
  <div className={classNames({ 'navbar-menu': true, 'is-active': isActive })}>
    <div className="navbar-start">
      {items.map(item => (
        <Item key={item.path} navItem={item} pathName={location.pathname} />
      ))}
    </div>
    <div className="navbar-end">
      {!!auth.isAuthenticated && (
        <div className="navbar-item">
          <div className="field is-grouped is-grouped-multiline">
            <p className="control">
              <button className="button" onClick={handleLogOut}>
                <span className="icon">
                  <i className="fas fa-sign-out-alt" />
                </span>
                <span>Logout</span>
              </button>
            </p>
          </div>
        </div>
      )}
    </div>
  </div>
)

export default Menu
