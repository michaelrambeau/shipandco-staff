import React from 'react'
import { withRouter } from 'react-router'

import OrderTableRow from './OrderTableRow'

const defaultOptions = {
  showIcon: true
}

const OrderListPage = ({
  orders,
  count,
  options = defaultOptions,
  history
}) => {
  if (!orders || orders.length === 0) return <div>No order!</div>
  return (
    <div>
      <table className="table is-striped clickable">
        <thead>
          <tr>
            {options.showIcon && <th />}
            <th>Customer</th>
            <th>Total</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {orders.map(order => (
            <OrderTableRow
              order={order}
              key={order._id}
              options={options}
              history={history}
            />
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default withRouter(OrderListPage)
