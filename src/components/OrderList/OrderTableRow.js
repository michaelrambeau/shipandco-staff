import React from 'react'
import tinytime from 'tinytime'

import TimeAgo from '../../components/utils/TimeAgo'
import Flag from '../../components/utils/Flag'
import ShopIcon from '../../components/utils/ShopIcon'
import Amount from '../../components/utils/Amount'
import { getFullname } from '../../helpers/addresses'

const template = tinytime('{YYYY}/{Mo}/{DD} {H}:{mm}', {
  padMonth: true,
  padDays: true
})

const goToOrder = history => order => () => history.push(`/orders/${order._id}`)

const OrderTableRow = ({ order, options, history }) => {
  const onClickRow = goToOrder(history)
  const totalPrice = order.products.reduce(
    (acc, product) => acc + product.price,
    0
  )
  return (
    <tr onClick={onClickRow(order)}>
      {options.showIcon && (
        <td width="52">
          <ShopIcon type={order.meta.type} />
        </td>
      )}
      <td>
        {getFullname(order.to_address)}
        <br />
        <Flag countryCode={order.to_address.country} />{' '}
        {order.to_address.city || order.to_address.address1}
        <div className="light-text">{order.meta.identifier}</div>
      </td>
      <td>
        <Amount value={totalPrice} currency={order.meta.currency} />
      </td>
      <td>
        <TimeAgo datetime={order.meta.created_at} />
        <br />
        {template.render(new Date(order.meta.created_at))}
      </td>
    </tr>
  )
}

export default OrderTableRow
