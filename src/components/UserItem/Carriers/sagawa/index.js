import React from 'react'
// import tinytime from 'tinytime'

// const template = tinytime('{YYYY} {MM} {DD} {H}:{mm}')

const Sagawa = ({ carrier }) => {
  const { credentials } = carrier
  const { account_number, account_key_number, key } = credentials
  return (
    <div>
      <div className="field">
        <label className="label">Account Number</label>
        <p className="control">{account_number}</p>
      </div>
      <div className="field">
        <label className="label">Account Key Number</label>
        <p className="control">{account_key_number}</p>
      </div>
      <div className="field">
        <label className="label">Key</label>
        <p className="control">{key}</p>
      </div>
    </div>
  )
}

export default Sagawa
