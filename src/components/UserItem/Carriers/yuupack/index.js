import React from 'react'
import PropTypes from 'prop-types'

const Yuupack = ({ carrier }) => {
  const { user_id } = carrier.credentials
  return <div>{user_id}</div>
}

Yuupack.propTypes = {
  carrier: PropTypes.object.isRequired
}

export default Yuupack
