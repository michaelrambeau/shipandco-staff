import React from 'react'
import PropTypes from 'prop-types'

const Ups = ({ carrier }) => {
  const { credentials } = carrier
  const { account_number, user_name } = credentials
  return (
    <div>
      <div className="field">
        <label className="label">Account Number</label>
        <p className="control">{account_number}</p>
      </div>
      <div className="field">
        <label className="label">Username</label>
        <p className="control">{user_name}</p>
      </div>
    </div>
  )
}

Ups.propTypes = {
  carrier: PropTypes.object.isRequired
}

export default Ups
