import React from 'react'
import PropTypes from 'prop-types'

const Fedex = ({ carrier }) => {
  const { credentials } = carrier
  const { account_number, meter_number } = credentials
  return (
    <div>
      <div className="field">
        <label className="label">Account Number</label>
        <p className="control">{account_number}</p>
      </div>
      <div className="field">
        <label className="label">Metric Number</label>
        <p className="control">{meter_number}</p>
      </div>
    </div>
  )
}

Fedex.propTypes = {
  carrier: PropTypes.object.isRequired,
}

export default Fedex
