import React from 'react'

import CarrierItem from './CarrierItem'

export default ({ carriers }) => {
  const enabledCarriers = carriers.filter(
    carrier => carrier.meta.enabled !== false
  )
  if (enabledCarriers.length === 0) {
    return (
      <div className="box">
        <h4 className="title is-4">Carriers</h4>
        <div>No carrier</div>
      </div>
    )
  }
  return (
    <div className="columns is-multiline">
      {enabledCarriers.map(carrier => (
        <CarrierItem key={carrier.meta.type} carrier={carrier} />
      ))}
    </div>
  )
}
