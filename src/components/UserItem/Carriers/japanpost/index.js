import React from 'react'
import PropTypes from 'prop-types'

const Numbers = ({ numbers }) => {
  if (!numbers) return <div>No numbers</div>
  if (!Array.isArray(numbers)) return <div>Invalid numbers!</div>
  if (numbers.every(number => number === ''))
    return <div className="empty">No numbers</div>
  return <div>{numbers.join(' - ')}</div>
}

const JapanPost = ({ carrier }) => {
  const numbers = carrier.credentials.customer_numbers
  return <Numbers numbers={numbers} />
}

JapanPost.propTypes = {
  carrier: PropTypes.object.isRequired
}

export default JapanPost
