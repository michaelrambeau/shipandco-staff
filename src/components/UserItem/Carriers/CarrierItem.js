import React from 'react'

import integrations from '../../../data/carriers'

import CarrierIcon from '../../utils/CarrierIcon'
import JapanPost from './japanpost'
import Fedex from './fedex'
import Dhl from './dhl'
import Ups from './ups'
import Yamato from './yamato'
import Yuupack from './yuupack'
import Sagawa from './sagawa'

const carriers = {
  yuupack: Yuupack,
  yuupacket: Yuupack,
  japanpost: JapanPost,
  yamato: Yamato,
  sagawa: Sagawa,
  ups: Ups,
  dhl: Dhl,
  fedex: Fedex,
}

const CarrierItem = ({ carrier }) => {
  const type = carrier.meta.type
  const CarrierComponent = carriers[type]
  if (!CarrierComponent) throw new Error(`No carrier component "${type}"`)
  const integration = integrations[type]
  if (!integration) throw new Error(`No carrier integration "${type}"`)
  return (
    <div className="column is-half-tablet">
      <div className="box" style={{ height: '100%' }}>
        <div className="media">
          <div className="media-left">
            <CarrierIcon type={type} />
          </div>
          <div className="media-content">
            <h4 className="title is-5">{integration.name}</h4>
            <div>
              <CarrierComponent carrier={carrier} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CarrierItem
