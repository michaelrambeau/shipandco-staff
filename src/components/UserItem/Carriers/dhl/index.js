import React from 'react'
import PropTypes from 'prop-types'

const Dhl = ({ carrier }) => {
  const { credentials } = carrier
  const { account_number } = credentials
  if (!account_number) return <div>No DHL Account Number</div>
  return (
    <div className="field">
      <label className="label">Account Number</label>
      <p className="control">{account_number}</p>
    </div>
  )
}

Dhl.propTypes = {
  carrier: PropTypes.object.isRequired,
}

export default Dhl
