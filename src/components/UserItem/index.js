import React from 'react'
import { Route, withRouter } from 'react-router-dom'

import Tabs from './Tabs'
import Profile from './Profile'
import UserShops from './Shops'
import UserOrders from './Orders'
import UserShipments from './Shipments'
import UserBilling from './Billing'

// HoF to ddd the `user` property to any component
const withUser = user => WrappedComponent => props => (
  <WrappedComponent {...this.props} user={user} />
)

// React Router `location` props:
// {
//   "pathname": "/users/S9iW6pZMG8x2WatXK/shops",
//   "search": "",
//   "hash": "",
//   "key": "pfgznq"
// }

// React Router `match` props:
// {
//   "path": "/users/:id",
//   "url": "/users/S9iW6pZMG8x2WatXK",
//   "isExact": false,
//   "params": {
//     "id": "S9iW6pZMG8x2WatXK"
//   }
// }

const User = ({ user, match, location }) => {
  if (!user) return <div>No user found!</div>
  return (
    <div style={{ marginBottom: '1rem' }}>
      <h2 className="title is-3">
        {user.emails[0].address}
        {user.free_shipments > 0 && (
          <div
            className={`tag is-large${
              user.free_shipments === 10 ? '' : ' is-info'
            }`}
            style={{ marginLeft: '.5rem' }}
          >
            {user.free_shipments} free shipments remaining
          </div>
        )}
      </h2>
      <Tabs user={user} userId={user._id} path={location.pathname} />
      <Route
        exact
        path={`${match.url}`}
        component={withUser(user)(Profile)}
        state={{ key: 'profile' }}
      />
      <Route
        exact
        path={`${match.url}/shops`}
        component={withUser(user)(UserShops)}
        state={{ key: 'shops' }}
      />
      <Route
        exact
        path={`${match.url}/orders`}
        component={withUser(user)(UserOrders)}
      />
      <Route
        exact
        path={`${match.url}/shipments`}
        component={withUser(user)(UserShipments)}
      />
      <Route
        exact
        path={`${match.url}/billing`}
        component={withUser(user)(UserBilling)}
      />
    </div>
  )
}

export default withRouter(User)
