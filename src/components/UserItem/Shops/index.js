import React from 'react'

import ShopList from '../../ShopList'

export default ({ user }) => {
  if (!user) return <div>No user!</div>
  return <ShopList shops={user.shops} count={user.shops.length} />
}
