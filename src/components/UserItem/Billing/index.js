import React from 'react'

import FetchList from '../../../containers/FetchList'
import UserBillingInfo from './UserBillingInfo'
import UserInvoiceList from './UserInvoiceList'

const UserBilling = ({ user }) => {
  if (!user) return <div>No user!</div>
  const query = { userId: user._id }
  const { billing } = user
  return (
    <div>
      <UserBillingInfo billing={billing} />
      <FetchList endPoint={'payments'} query={query}>
        {({ data }) => <UserInvoiceList invoices={data} />}
      </FetchList>
    </div>
  )
}

export default UserBilling
