import React from 'react'

import FetchList from '../../../containers/FetchList'
import ListWithPagination from '../../../containers/ListWithPagination'
import PaginationContainer from '../../../containers/PaginationContainer'

import OrderList from '../../OrderList'

const UserOrderListPage = ({ user }) => {
  const endPoint = 'orders'
  return (
    <PaginationContainer>
      {({ pageIndex, $skip, $limit }) => {
        const query = { $limit, $skip, 'meta.user_id': user._id }
        return (
          <FetchList endPoint={endPoint} query={query}>
            {({ total, data }) => (
              <ListWithPagination
                pageIndex={pageIndex}
                total={total}
                skip={$skip}
                limit={$limit}
                endPoint={endPoint}
              >
                {() => <OrderList orders={data} count={total} />}
              </ListWithPagination>
            )}
          </FetchList>
        )
      }}
    </PaginationContainer>
  )
}

export default UserOrderListPage
