import React from 'react'
import { getFullname } from '../../../helpers/addresses'

const Item = ({ label, children }) => (
  <div className="field">
    <label className="label">{label}</label>
    <p className="control">{children}</p>
  </div>
)

const Contact = ({ contact }) => {
  if (!contact)
    return (
      <div className="message is-warning">
        <div className="message-body">No Contact Information</div>
      </div>
    )
  const fullname = getFullname(contact)
  return (
    <div className="box">
      <h3 className="title is-4">Contact</h3>
      <Item label="Name">{fullname}</Item>
      <Item label="Company">{contact.company}</Item>
      <Item label="Phone">{contact.phone}</Item>
      <Item label="Email">{contact.email}</Item>
      <Item label="Address 1">{contact.address1}</Item>
      <Item label="Address 2">{contact.address2}</Item>
      <Item label="Province">{contact.province}</Item>
      <Item label="Zip">{contact.zip}</Item>
    </div>
  )
}

export default Contact
