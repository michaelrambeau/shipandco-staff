import React from 'react'

import Flag from '../../utils/Flag'

const languageSettings = {
  'ja-JP': {
    name: 'Japanese',
    flag: 'jp'
  },
  'en-UK': {
    name: 'English',
    flag: 'us'
  },
  'fr-FR': {
    name: 'French',
    flag: 'fr'
  }
}

const Language = ({ languageKey }) => {
  const settings = languageSettings[languageKey]
  if (!settings) return <span>{languageKey}</span>
  return (
    <span>
      <Flag countryCode={settings.flag} /> {settings.name}
    </span>
  )
}

const Item = ({ label, children }) => (
  <div className="field">
    <label className="label">{label}</label>
    <p className="control">
      {children === '' ? <span className="empty">(empty)</span> : children}
    </p>
  </div>
)

const Preferences = ({ preferences }) => {
  if (!preferences)
    return (
      <div className="message is-warning">
        <div className="message-header">Preferences</div>
        <div className="message-body">No preferences</div>
      </div>
    )
  return (
    <div className="box">
      <h3 className="title is-4">Preferences</h3>
      <Item label="Language">
        <Language languageKey={preferences.language} />
      </Item>
      <Item label="Weight unit">{preferences.weight_unit}</Item>
      <Item label="Default content type">
        {preferences.default_content_type}
      </Item>
      <Item label="Default discount rate">
        {preferences.default_discount_rate} %
      </Item>
      <Item label="Signature request">
        {preferences.default_signature_request ? 'YES' : 'NO'}
      </Item>
      <Item label="Export file encoding">
        {preferences.export_file_encoding}
      </Item>
    </div>
  )
}

export default Preferences
