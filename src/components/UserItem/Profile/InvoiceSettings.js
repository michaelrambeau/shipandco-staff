import React from 'react'

const Item = ({ label, children }) => (
  <div className="field">
    <label className="label">{label}</label>
    <p className="control">
      {children === '' ? <span className="empty">(empty)</span> : children}
    </p>
  </div>
)

const InvoiceSettings = ({ invoice }) => {
  if (!invoice)
    return (
      <div className="message is-warning">
        <div className="message-body">No Invoice Settings</div>
      </div>
    )
  return (
    <div className="box">
      <h3 className="title is-4">Invoice Settings</h3>
      <Item label="Group products by type">
        {invoice.group_products ? 'YES' : 'NO'}
      </Item>
      <Item label="Use category description instead of product name">
        {invoice.use_description ? 'YES' : 'NO'}
      </Item>
      <Item label="Message">{invoice.message}</Item>
      <Item label="Message (kanji)">{invoice.message_kanji}</Item>
    </div>
  )
}

export default InvoiceSettings
