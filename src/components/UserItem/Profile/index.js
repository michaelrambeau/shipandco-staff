import React from 'react'
import get from 'lodash.get'

import ProfileView from './UserProfile'
import GoodCategories from './GoodCategories'
import OnBoarding from './OnBoarding'
import Contact from './Contact'
import InvoiceSettings from './InvoiceSettings'
import Preferences from './Preferences'
import CustomParcels from './CustomParcels'
import CarrierList from '../Carriers/CarrierList'
import WarehouseList from '../../WarehouseList'

const Profile = ({ user }) => {
  const onBoardingFlags = get(user, 'flags.onboarding') || []
  const warehouses = user.warehouses || []
  const settings = user.settings
  const { invoice, preferences } = settings
  const goodCategories = get(user, 'settings.goods_types') || []
  const parcels = get(user, 'settings.custom_parcels') || []
  return (
    <div>
      <div className="columns">
        <div className="column is-half-tablet">
          <ProfileView user={user} profile={user.profile} />
          <OnBoarding onBoardingFlags={onBoardingFlags} />
        </div>
        <div className="column is-half-tablet">
          <Contact user={user} contact={user.contact} />
        </div>
      </div>
      <CarrierList carriers={user.carriers} />
      <div className="box">
        <h4 className="title is-4">Warehouses</h4>
        <WarehouseList warehouses={warehouses} count={warehouses.length} />
      </div>
      {settings ? (
        <div>
          <div className="columns">
            <div className="column is-half-tablet">
              <Preferences preferences={preferences} />
            </div>
            <div className="column is-half-tablet">
              <InvoiceSettings invoice={invoice} />
            </div>
          </div>
          <div className="columns">
            <div className="column is-half-tablet">
              <GoodCategories categories={goodCategories} />
            </div>
            <div className="column is-half-tablet">
              <CustomParcels parcels={parcels} />
            </div>
          </div>
        </div>
      ) : (
        <div>No Settings</div>
      )}
    </div>
  )
}

export default Profile
