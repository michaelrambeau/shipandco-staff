import React from 'react'
import tinytime from 'tinytime'

const template = tinytime('{YYYY}/{Mo}/{DD}', {
  padMonth: true,
  padDays: true
})

const Item = ({ label, children }) => (
  <div className="field">
    <label className="label">{label}</label>
    <p className="control">{children}</p>
  </div>
)

const formatDate = d => template.render(new Date(d))

const ProfileView = ({ user, profile }) => (
  <div className="box">
    <h3 className="title is-4">General</h3>
    <Item label="Email">{user.emails[0].address}</Item>
    <Item label="Registration date">{formatDate(user.createdAt)}</Item>
    {user.lastLogin && (
      <Item label="Last login">{formatDate(user.lastLogin)}</Item>
    )}
  </div>
)

export default ProfileView
