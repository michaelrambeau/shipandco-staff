import React from 'react'

const CustomParcels = ({ parcels }) => {
  if (parcels.length === 0)
    return (
      <div className="message is-warning">
        <div className="message-body">No Custom Packages!</div>
      </div>
    )
  return (
    <div className="box">
      <h3 className="title is-4">Custom Packages ({parcels.length})</h3>
      {parcels.length > 0 ? (
        <table className="table is-striped">
          <thead>
            <tr>
              <td>Name</td>
              <td>Dimensions (cm)</td>
              <td>Weight (g)</td>
            </tr>
          </thead>
          <tbody>
            {parcels.map((parcel, i) => <Row key={i} parcel={parcel} />)}
          </tbody>
        </table>
      ) : (
        <div className="">No custom parcels</div>
      )}
    </div>
  )
}

const Row = ({ parcel }) => (
  <tr>
    <td>{parcel.name}</td>
    <td>
      {parcel.width} * {parcel.depth} * {parcel.height}
    </td>
    <td>{parcel.weight}</td>
  </tr>
)

export default CustomParcels
