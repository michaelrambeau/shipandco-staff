import React from 'react'
import { Link } from 'react-router-dom'

const Counter = ({ count }) => {
  const value = count === undefined ? 'Loading...' : count
  if (count === 0) return null
  return <span className="light-text">{value}</span>
}

const items = [
  {
    path: '',
    text: ({ user }) => <span>Profile</span>,
    icon: 'user',
    enabled: () => true
  },
  {
    path: '/shops',
    text: ({ user }) => (
      <span>
        Shops <Counter count={user.shops.length} />
      </span>
    ),
    icon: 'shopping-bag',
    enabled: () => true
  },
  {
    path: '/orders',
    text: ({ user }) => (
      <span>
        Orders <Counter count={user.orderCount} />
      </span>
    ),
    icon: 'list-ul',
    enabled: () => true
  },
  {
    path: '/shipments',
    text: ({ user }) => (
      <span>
        Shipments <Counter count={user.shipmentCount} />
      </span>
    ),
    icon: 'cubes',
    enabled: () => true
  },
  {
    path: '/billing',
    text: ({ user }) => <span>Billing</span>,
    icon: 'credit-card',
    enabled: user => !!user.billing
  }
]

const Tabs = ({ userId, path, user }) => {
  return (
    <div className="tabs is-boxed">
      <ul>
        {items
          .filter(item => item.enabled(user))
          .map(item => (
            <Tab
              key={item.path}
              item={item}
              userId={userId}
              path={path}
              user={user}
            />
          ))}
      </ul>
    </div>
  )
}

const Tab = ({ item, userId, user, path }) => {
  const Text = item.text
  const targetPath = `/users/${userId}${item.path}`
  const isActive = targetPath === path
  return (
    <li className={isActive ? 'is-active' : ''}>
      <Link to={`/users/${userId}${item.path}`}>
        <span className="icon is-small">
          <i className={`fas fa-${item.icon}`} />
        </span>
        <Text user={user} />
      </Link>
    </li>
  )
}

export default Tabs
