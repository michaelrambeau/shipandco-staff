import React from 'react'
import get from 'lodash.get'
import classNames from 'classnames'

import TimeAgo from '../../components/utils/TimeAgo'
import ShopIcon from '../../components/utils/ShopIcon'
// import CarrierIcon from '../../components/utils/CarrierIcon'
import CreditCardIcon from '../../components/utils/CreditCardIcon'

import FreeShipments from './FreeShipments'
import goToUser from './goToUser'
import LastLogin from './columns/LastLogin'
import { getFullname } from '../../helpers/addresses'

const UserTableRow = ({ user, history }) => {
  const onClickRow = goToUser(history)
  return (
    <tr
      onClick={onClickRow(user)}
      className={classNames({ 'is-active-row': !!user.contact })}
    >
      <td>
        {user.emails[0].address}
        {!user.emails[0].verified && (
          <span style={{ marginLeft: 5, color: '#ff3860' }}>Not verified</span>
        )}
        <br />
        <ContactName user={user} />
      </td>
      <td>
        <ShopList shops={user.shops} />
      </td>
      <td>
        <TimeAgo datetime={user.createdAt} />
        <br />
        <FreeShipments count={user.free_shipments} />
      </td>
      <td>
        <Billing user={user} />
      </td>
      <td>
        <LastLogin user={user} />
      </td>
    </tr>
  )
}
export default UserTableRow

const ContactName = ({ user }) => {
  const { contact } = user
  if (!contact) return <span className="empty">(no contact)</span>
  const { email } = contact
  return (
    <div>
      {getFullname(contact)}
      {email !== user.emails[0].address && <div>{email}</div>}
    </div>
  )
}

const ShopList = ({ shops }) => {
  if (shops.length === 0) return <span className="empty">No shop</span>
  return (
    <div>
      {shops.map(shop => {
        return (
          <div key={shop._id}>
            <ShopIcon type={shop.meta.type} size={24} /> {shop.meta.identifier}
          </div>
        )
      })}
    </div>
  )
}

const Billing = ({ user }) => {
  const { billing } = user
  const brand = get(billing, 'card.brand')
  if (!brand) return <div className="empty">No CC</div>
  return (
    <div>
      <CreditCardIcon brand={brand} />
    </div>
  )
}
