// import { browserHistory as history } from 'react-router'

const goToUser = history => user => {
  return () => history.push(`/users/${user._id}`)
}
export default goToUser
