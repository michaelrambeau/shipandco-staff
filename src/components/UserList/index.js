import React from 'react'
import { withRouter } from 'react-router-dom'

import UserTableRow from './UserTableRow'
import './styles.scss'

const defaultOptions = {
  showHeader: true,
}

const UserList = ({ users, history, options = defaultOptions }) => {
  return (
    <table className={`table is-striped clickable`}>
      {options.showHeader && (
        <thead>
          <tr>
            <th>Email / Name</th>
            <th>Shops</th>
            <th>Registration</th>
            <th>Card</th>
            <th>Login</th>
          </tr>
        </thead>
      )}
      <tbody>
        {users.map(user => (
          <UserTableRow
            user={user}
            key={user._id}
            history={history}
            options={options}
          />
        ))}
      </tbody>
    </table>
  )
}
export default withRouter(UserList)
