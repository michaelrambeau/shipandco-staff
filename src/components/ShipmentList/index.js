import React from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

import ShipmentTableRow from './ShipmentTableRow'

const defaultOptions = {
  showIcon: true,
  showRate: true,
  showHeader: true,
  compact: false,
  showLabelStatus: true,
  showBillingStatus: true,
}

const Table = ({ shipments, count, history, options = defaultOptions }) => {
  if (!shipments || shipments.length === 0) return <div>No shipment!</div>
  return (
    <div>
      <table className="table is-striped clickable">
        {options.showHeader && (
          <thead>
            <tr>
              {options.showIcon && <th />}
              <th>Customer</th>
              <th>Tracking #</th>
              {options.showLabelStatus && <th>Label</th>}
              {options.showBillingStatus && <th>Invoice</th>}
              {options.showRate && <th>Rate</th>}
              <th>Date</th>
            </tr>
          </thead>
        )}
        <tbody
          style={{ borderTop: options.showHeader ? '' : '1px solid #dbdbdb' }}
        >
          {shipments.map(shipment => (
            <ShipmentTableRow
              shipment={shipment}
              key={shipment._id}
              options={options}
              history={history}
              compact={options.compact}
            />
          ))}
        </tbody>
      </table>
    </div>
  )
}

Table.propTypes = {
  shipments: PropTypes.array.isRequired,
  count: PropTypes.number.isRequired,
}
export default withRouter(Table)
