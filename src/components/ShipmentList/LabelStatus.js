import React from 'react'
import get from 'lodash.get'

import './styles.css'

const JapanPostAirmail = () => (
  <span className="japanpost-label japanpost-airmail">AIR</span>
)
const JapanPostSAL = () => (
  <span className="japanpost-label japanpost-sal">SAL</span>
)

const labels = {
  sal: JapanPostSAL,
  airmail: JapanPostAirmail,
}

const LabelStatus = ({ shipment, options }) => {
  const shipmentState = shipment.state
  const status = get(shipment, 'label.status') || ''
  const method = get(shipment, 'delivery.method') || ''
  if (shipmentState === 'void') return <div className="tag is-danger">VOID</div>
  if (status === 'removed')
    return <i className="fas fa-trash-alt light-text" aria-hidden="true" />
  if (status === 'error')
    return <i className="fas fa-database error-icon" aria-hidden="true" />
  if (status === 'no-label') {
    const Label = labels[method] || (() => <div>{method}</div>)
    return <Label />
  }

  if (status === 'uploaded')
    return (
      <span className="icon">
        <i className="fas fa-cloud light-text" aria-hidden="true" />
      </span>
    )
  return (
    <span className="icon">
      <i className="fas fa-database light-text" aria-hidden="true" />
    </span>
  )
}

export default LabelStatus
