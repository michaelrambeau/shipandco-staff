import React from 'react'
import PropTypes from 'prop-types'
import tinytime from 'tinytime'
import get from 'lodash.get'

import TimeAgo from '../../components/utils/TimeAgo'
import Flag from '../../components/utils/Flag'
import CarrierIcon from '../../components/utils/CarrierIcon'
import ShopIcon from '../../components/utils/ShopIcon'
import Amount from '../../components/utils/Amount'
import LabelStatus from './LabelStatus'
import { getFullname } from '../../helpers/addresses'
import { limitString } from '../../helpers/strings'

const templates = {
  full: tinytime('{MM} {DD} {H}:{mm}', {
    padMonth: true,
    padDays: true,
  }),
  timeOnly: tinytime('{H}:{mm}'),
  invoiceDate: tinytime('{MM} {DD}', {
    padMonth: true,
    padDays: true,
  }),
}

function shipmentDate(date) {
  const today = new Date()
  const sameDay =
    today.getDay() === date.getDay() && today.getMonth() === date.getMonth()
  const template = sameDay ? templates.timeOnly : templates.full
  return template.render(date)
}

const goToShipment = history => shipment => () =>
  history.push(`/shipments/${shipment._id}`)

const ShipmentTableRow = ({ shipment, options, history, compact }) => {
  const onClickRow = goToShipment(history)
  const method = (get(shipment, 'delvirery.method') || '').toUpperCase()
  const trackingNumber = get(shipment, 'delivery.tracking_numbers[0]') || ''
  const { identifier } = shipment.meta
  const displayedIdentifier = compact ? limitString(20, identifier) : identifier
  return (
    <tr onClick={onClickRow(shipment)}>
      {options.showIcon && (
        <td width="52">
          <ShopIcon type={shipment.meta.type} />
        </td>
      )}
      <td>
        {getFullname(shipment.to_address)}
        <br />
        <Flag countryCode={shipment.to_address.country} />{' '}
        {shipment.to_address.city || shipment.to_address.address1}
        <div className="light-text">{displayedIdentifier}</div>
      </td>
      <td>
        <CarrierIcon type={shipment.delivery.carrier} />
        {!options.compact && (
          <div>
            <span>{method}</span>
            <br />
            <span>{trackingNumber}</span>
          </div>
        )}
      </td>
      {options.showLabelStatus && (
        <td>
          <LabelStatus shipment={shipment} />
        </td>
      )}
      {options.showBillingStatus && (
        <td>
          <BillingIcon shipment={shipment} />
        </td>
      )}
      {options.showRate && (
        <td>
          <Amount
            value={get(shipment, 'delivery.amount') || 0}
            currency={get(shipment, 'delivery.currency') || ''}
          />
        </td>
      )}
      <td>
        {!options.compact && (
          <div>
            <TimeAgo datetime={shipment.meta.created_at} />
          </div>
        )}
        {shipmentDate(new Date(shipment.meta.created_at))}
      </td>
    </tr>
  )
}

const BillingIcon = ({ shipment }) => {
  const { meta } = shipment
  if (!meta) return null
  const { invoiced_at, free } = meta
  if (invoiced_at)
    return (
      <div>
        <span className="fas fa-check-square light-text" />{' '}
        {templates.invoiceDate.render(new Date(invoiced_at))}
      </div>
    )
  if (free)
    return (
      <div>
        <span className="fas fa-gift light-text" /> Free!
      </div>
    )
  return null
}

ShipmentTableRow.propTypes = {
  shipment: PropTypes.object.isRequired,
}

export default ShipmentTableRow
