import React from 'react'
import { Link } from 'react-router-dom'

import ShopIcon from '../../utils/ShopIcon'

const Buttons = ({ record, model }) => (
  <div className="text-center">
    <Link to={`/users/${record.meta.user_id}/${model}`} className="button">
      <span className="icon">
        <i className="fa fa-user" />
      </span>
      <span>Go to the customer</span>
    </Link>
    {record.meta.shop_id && (
      <Link
        to={`/shops/${record.meta.shop_id}`}
        className="button"
        style={{ marginLeft: '1.5rem' }}
      >
        <span className="icon">
          <ShopIcon type={record.meta.type} />
        </span>
        <span>Go to the shop</span>
      </Link>
    )}
  </div>
)

export default Buttons
