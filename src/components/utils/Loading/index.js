import React from 'react'

const Loading = ({ text = 'Loading...' }) => (
  <div className="container">
    <section className="section">
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <div>
          <div className="loader" />
        </div>
        <div style={{ color: '#999', padding: 10 }}>{text}</div>
      </div>
    </section>
  </div>
)

export default Loading
