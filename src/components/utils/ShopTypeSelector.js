import React from 'react'
import { Link } from 'react-router-dom'

import shopTypes from '../../data/shops'

const shopTypeItems = Object.keys(shopTypes).map(key => ({
  id: key,
  ...shopTypes[key],
}))

const ShopTypeSelector = ({ baseUrl, current }) => (
  <div className="tabs">
    <ul>
      <li className={!current ? 'is-active' : ''}>
        <Link to={`${baseUrl}`}>All</Link>
      </li>
      {shopTypeItems.map(shop => (
        <ShopTypeItem
          shop={shop}
          key={shop.value}
          baseUrl={baseUrl}
          isActive={current === shop.value}
          count={0}
        />
      ))}
    </ul>
  </div>
)

const ShopTypeItem = ({ shop, baseUrl, isActive, count }) => (
  <li className={isActive ? 'is-active' : ''}>
    <Link to={`${baseUrl}meta.type=${shop.value}`}>
      <span>{shop.text}</span>
      {count > 0 && <span className="tag is-rounded">{count}</span>}
    </Link>
  </li>
)

export default ShopTypeSelector
