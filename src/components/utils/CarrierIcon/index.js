import React from 'react'

import dhl from './dhl.svg'
import fedex from './fedex.svg'
import ups from './ups.svg'
import japanpost from './japanpost.svg'
import yuupack from './yuupack.svg'
import yamato from './yamato.svg'
import sagawa from './sagawa.svg'

function getIcon(carrier) {
  switch (carrier) {
    case 'dhl':
      return dhl
    case 'fedex':
      return fedex
    case 'ups':
      return ups
    case 'japanpost':
      return japanpost
    case 'yuupack':
    case 'yuupacket':
      return yuupack
    case 'yamato':
      return yamato
    case 'sagawa':
      return sagawa
    default:
      return null
  }
}

const CarrierIcon = ({ type, size = 32 }) => {
  const icon = getIcon(type)
  if (!icon) return <span>{type}</span>
  return (
    <img
      src={icon}
      width={size}
      height={size}
      style={{ verticalAlign: 'middle', marginRight: '.5rem' }}
      alt={type}
    />
  )
}

export default CarrierIcon
