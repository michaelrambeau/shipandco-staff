import React from 'react'
import get from 'lodash.get'

import ShopIcon from '../utils/ShopIcon'
import UserList from '../UserList'
import OrderList from '../OrderList'
import ShipmentList from '../ShipmentList'
import WarehouseList from '../WarehouseList'
import Settings from './Settings'
import SubShops from './SubShops'

const viewOptions = {
  showOptions: false,
}

const ViewWarehouse = ({ warehouse }) =>
  warehouse.deleted ? (
    <div>The linked warehouse ({warehouse._id}) has been deleted</div>
  ) : (
    <WarehouseList warehouses={[warehouse]} count={1} />
  )

const Shop = ({ shop }) => {
  const users = [shop.user]
  const orders = get(shop, 'orders.data') || []
  const orderCount = get(shop, 'orders.total') || 0
  const shipments = get(shop, 'shipments.data') || []
  const shipmentCount = get(shop, 'shipments.total') || 0
  return (
    <div>
      <h2 className="title is-4">
        <ShopIcon type={shop.meta.type} /> Shop {shop.meta.identifier}
      </h2>

      <Settings shop={shop} />

      {shop.shops && <SubShops shops={shop.shops} />}

      <div className="card" style={{ marginBottom: '2rem' }}>
        <div className="card-header">
          <h3 className="card-header-title">Customer</h3>
        </div>
        <div className="card-content">
          {shop.user && users.length > 0 && <UserList users={users} />}
        </div>
      </div>

      <div className="card" style={{ marginBottom: '2rem' }}>
        <div className="card-header">
          <h3 className="card-header-title">Warehouse</h3>
        </div>
        <div className="card-content">
          {shop.warehouse ? (
            <ViewWarehouse warehouse={shop.warehouse} />
          ) : (
            <div>No warehouse linked to this shop.</div>
          )}
        </div>
      </div>

      <div className="card" style={{ marginBottom: '2rem' }}>
        <header className="card-header">
          <h3 className="card-header-title">Orders ({orderCount})</h3>
        </header>
        <div className="card-content">
          <OrderList orders={orders} count={orderCount} options={viewOptions} />
        </div>
      </div>

      <div className="card" style={{ marginBottom: '2rem' }}>
        <header className="card-header">
          <h3 className="card-header-title">Shipments ({shipmentCount})</h3>
        </header>
        <div className="card-content">
          <ShipmentList
            shipments={shipments}
            count={shipmentCount}
            options={viewOptions}
          />
        </div>
      </div>
    </div>
  )
}

export default Shop
