import React from 'react'
import tinytime from 'tinytime'

import TimeAgo from '../utils/TimeAgo'

// const settingsKeys = ['licenseKey']
// const excludedKeys = ['warehouseId']

const template = tinytime('{MM} {DD} {H}:{mm}', {
  padMonth: true,
  padDays: true,
})

const Item = ({ label, children }) => (
  <div className="field is-horizontal">
    <div className="field-label">
      <label className="label">{label}</label>
    </div>
    <div className="field-body">
      <div className="field">
        <p className="control">{children}</p>
      </div>
    </div>
  </div>
)

const Settings = ({ shop }) => (
  <div className="card" style={{ marginBottom: '2rem' }}>
    <header className="card-header">
      <h4 className="card-header-title">Settings</h4>
    </header>
    <div className="card-content">
      <Item label="Added">
        <TimeAgo datetime={shop.meta.created_at} />
        {' ('}
        {template.render(new Date(shop.meta.created_at))}
        {')'}
      </Item>
      <Item label="Auto-fulfill">
        {shop.settings.autofulfill ? (
          <span>
            <span className="fas fa-check-circle" />
            {' Yes'}
          </span>
        ) : (
          <span className="empty">No auto-fulfill</span>
        )}
      </Item>
      <Sync shop={shop} />
    </div>
  </div>
)

const Sync = ({ shop }) => {
  const sync = shop.sync
  if (!sync) return null
  return (
    <div>
      <Item label="Last sync.">
        <TimeAgo datetime={sync.synced_at} />
        {' ('}
        {template.render(new Date(sync.synced_at))}
        {')'}
      </Item>
    </div>
  )
}

// const KeyValueHash = ({ hash, keys }) => {
//   if (!hash) return null
//   const allKeys = keys || Object.keys(hash)
//   if (allKeys.length === 0) return null
//   return (
//     <ul>
//       {allKeys
//         .filter(key => !excludedKeys.includes(key))
//         .filter(key => hash[key] !== undefined)
//         .map(key => (
//           <li key={key}>
//             {key}: {JSON.stringify(hash[key])}
//           </li>
//         ))}
//     </ul>
//   )
// }

export default Settings
