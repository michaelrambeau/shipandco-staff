import React from 'react'

import AddressTo from './AddressTo'
import ParcelList from './ParcelList'
import ProductList from './ProductList'
import OrderShipmentHeader from '../utils/OrderShipmentHeader'
import OrderExtra from './OrderExtra'

const Order = ({ order }) => {
  const { products, parcels } = order
  return (
    <div>
      <OrderShipmentHeader
        model="orders"
        record={order}
        title={`Order ${order && order.meta.identifier}`}
      />
      {products.length > 0 && (
        <div className="card is-fullwidth" style={{ marginBottom: '2rem' }}>
          <div className="card-header">
            <div className="card-header-title">Products</div>
          </div>
          <div className="card-content">
            <ProductList products={products} currency={order.meta.currency} />
          </div>
        </div>
      )}
      <div className="columns">
        <div className="column is-half">
          <div className="card is-fullwidth">
            <div className="card-header">
              <div className="card-header-title">Shipping Address</div>
            </div>
            <div className="card-content">
              <AddressTo address={order.to_address} />
            </div>
          </div>
        </div>
        <div className="column is-half">
          <OrderExtra order={order} />
          {parcels.length > 0 && (
            <div className="card is-fullwidth">
              <div className="card-header">
                <div className="card-header-title">Parcels</div>
              </div>
              <div className="card-content">
                <ParcelList parcels={parcels} />
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default Order
