import React from 'react'

import Flag from '../utils/Flag'
import { getFullname } from '../../helpers/addresses'

const Item = ({ label, children }) => (
  <div style={{ marginBottom: '1rem' }}>
    <label className="label">{label}</label>
    <p className="control">{children}</p>
  </div>
)

const Empty = () => <span className="empty">(empty)</span>

const To = ({ address }) => {
  const fullName = getFullname(address)
  return (
    <div>
      <Item label="Name">{fullName}</Item>
      <Item label="Phone">{address.phone || <Empty />}</Item>
      <Item label="Email">{address.email || <Empty />}</Item>
      <Item label="Address #1">{address.address1 || <Empty />}</Item>
      <Item label="Address #2">{address.address2 || <Empty />}</Item>
      <Item label="City">{address.city || <Empty />}</Item>
      <Item label="Zip Code">{address.zip || <Empty />}</Item>
      <Item label="Province Code">{address.province || <Empty />}</Item>
      <Item label="Country">
        <Flag countryCode={address.country} /> {address.country}
      </Item>
    </div>
  )
}

export default To
