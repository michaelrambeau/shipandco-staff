import React from 'react'
import Amount from '../../components/utils/Amount'

const ProductList = ({ products, currency }) => (
  <table className="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>HS Code</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      {products.map((product, i) => (
        <Row product={product} key={i} currency={currency} />
      ))}
    </tbody>
  </table>
)

const Row = ({ product, currency }) => (
  <tr>
    <td>{product.name}</td>
    <td>
      {product.hs_code}
      <br />
      {product.hs_description}
      <br />
      {product.origin_country}
    </td>
    <td>
      <Amount value={product.price} currency={currency} />
    </td>
  </tr>
)

export default ProductList
