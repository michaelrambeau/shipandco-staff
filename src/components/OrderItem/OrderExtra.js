import React from 'react'
import Amount from '../../components/utils/Amount'

const Item = ({ label, children }) => (
  <div style={{ marginBottom: '1rem' }}>
    <label className="label">{label}</label>
    <div className="control">{children}</div>
  </div>
)

const OrderExtra = ({ order }) => {
  const { origin, meta } = order
  const { currency } = meta
  const { total, points, charge, shipping_amount, paid, discount } = origin
  return (
    <div className="card">
      <div className="card-header">
        <div className="card-header-title">Origin information</div>
      </div>
      <div className="card-content">
        <Item label="Total">
          <Amount value={total} currency={currency} />
        </Item>
        {paid ? (
          <Item label="Paid total">
            <Amount value={paid} currency={currency} />
          </Item>
        ) : null}
        {points ? <Item label="Points">{points} points</Item> : null}
        {charge ? <Item label="Charge">{charge}</Item> : null}
        {discount ? <Item label="Discount">{discount} %</Item> : null}
        <Item label="Shipping Amount">
          <Amount value={shipping_amount} currency={currency} />
        </Item>
      </div>
    </div>
  )
}

export default OrderExtra
