import React from 'react'

const ParcelTable = ({ parcels }) => (
  <table className="table">
    <thead>
      <tr>
        <th>Width</th>
        <th>Height</th>
        <th>Depth</th>
        <th>Weight</th>
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>{parcels.map((parcel, i) => <Row parcel={parcel} key={i} />)}</tbody>
  </table>
)

const Row = ({ parcel }) => (
  <tr>
    <td>{parcel.width} cm</td>
    <td>{parcel.height} cm</td>
    <td>{parcel.depth} cm</td>
    <td>{parcel.weight} g</td>
    <td>{parcel.amount}</td>
  </tr>
)

export default ParcelTable
