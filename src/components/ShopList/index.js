import React from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

import ShopTableRow from './ShopTableRow'

const ShopTable = ({ shops, count, history }) => {
  if (!shops || shops.length === 0) return <div>No shop!</div>
  return (
    <div>
      <table className="table clickable is-striped">
        <thead>
          <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Settings</th>
            <th>Created</th>
            <th>Last sync.</th>
          </tr>
        </thead>
        <tbody>
          {shops.map(shop => (
            <ShopTableRow shop={shop} key={shop._id} history={history} />
          ))}
        </tbody>
      </table>
    </div>
  )
}

ShopTable.propTypes = {
  shops: PropTypes.array.isRequired,
  count: PropTypes.number.isRequired,
}

export default withRouter(ShopTable)
