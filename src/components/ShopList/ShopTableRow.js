import React from 'react'
import get from 'lodash.get'
import PropTypes from 'prop-types'

import TimeAgo from '../../components/utils/TimeAgo'
import ShopIcon from '../../components/utils/ShopIcon'
import { isSyncLate } from '../../helpers/shops'

const goToShop = history => shop => () => history.push(`/shops/${shop._id}`)

const Autofulfill = ({ shop }) => {
  const enabled = get(shop, 'settings.autofulfill')
  if (!enabled) return null
  return (
    <span>
      <span className="fas fa-check-circle" />
      {' Auto-fulfill'}
    </span>
  )
}

const ShopState = ({ shop }) => {
  const { state } = shop
  if (!state) return null
  const mapping = {
    pending: 'is-warning',
  }
  const className = mapping[state] || 'is-success'
  return (
    <div>
      <span className={`tag ${className}`}>{state}</span>
    </div>
  )
}

const ShopTableRow = ({ shop, history }) => {
  const onClickRow = goToShop(history)
  const creationDate = shop.meta.created_at
  const syncDate = get(shop, 'sync.synced_at')
  const isLate = isSyncLate(shop)
  const isIgnored = !!get(shop, 'sync.ignore_warnings')
  return (
    <tr onClick={onClickRow(shop)}>
      <td>
        <ShopIcon type={shop.meta.type} />
      </td>
      <td>{shop.meta.identifier}</td>
      <td>
        <Autofulfill shop={shop} />
        <ShopState shop={shop} />
      </td>
      <td>
        {creationDate ? (
          <TimeAgo datetime={creationDate} />
        ) : (
          <span className="empty">-</span>
        )}
      </td>
      <td>
        {syncDate ? (
          <div>
            <div>
              <TimeAgo datetime={syncDate} />
            </div>
            {isLate && <div className="tag is-danger">Late sync!</div>}
            {isIgnored && (
              <div className="tag is-warning" style={{ marginLeft: '.5rem' }}>
                Ignored
              </div>
            )}
          </div>
        ) : (
          <span className="empty">-</span>
        )}
        {shop.tokenExpiration && (
          <div style={{ color: '#999' }}>
            Token expiration date: <TimeAgo datetime={shop.tokenExpiration} />
          </div>
        )}
      </td>
    </tr>
  )
}

ShopTableRow.propTypes = {
  shop: PropTypes.object.isRequired,
}

export default ShopTableRow
